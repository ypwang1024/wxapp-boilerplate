
function formatNumber(n) {
  const s = n.toString();
  return s[1] ? s : `0${s}`;
}

export function formatTime(date) {
  const year = date.getFullYear();
  const month = date.getMonth() + 1;
  const day = date.getDate();
  const hour = date.getHours();
  const minute = date.getMinutes();
  const second = date.getSeconds();

  return `${[year, month, day]
    .map(formatNumber)
    .join('/')} ${
    [hour, minute, second].map(formatNumber).join(':')}`;
}

export function developing() {
  wx.showModal({
    title: '开发中',
    content: '开发中',
  });
}

/**
 * 创建一个地址附带查询参数
 * @param  {String} baseUrl 基础地址
 * @param  {String} paramsStr 选填
 * @return {void}
 */
export function createUrl(baseUrl, paramsStr) {
  if (!paramsStr) {
    return baseUrl;
  }

  let subStr = '';
  // 没有问号
  if (baseUrl.lastIndexOf('?') === -1) {
    subStr = '?';
  } else {
    // 最后一个字符
    const lastChar = baseUrl.slice(-1);
    // 如果最后接在最后一个字符不是是?并且是&加上一个&
    if (lastChar !== '&' && lastChar !== '?') {
      subStr = '&';
    }
  }

  return baseUrl + subStr + paramsStr;
}
export default {
  formatTime,
};
