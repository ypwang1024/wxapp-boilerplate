import request from 'wxapp-request';
import env from '../../../../env';

export default function entPayToWalletApi(productType, amount) {
  return request.post({
    url: `${env.API_URL}/pay/common/ent_pay_to_wallet?productType=${productType}`,
    data: {
      amount,
    },
  });
}
