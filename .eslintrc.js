module.exports = {
  "root": true,
  "extends": "airbnb-base",
  "env": {
    "browser": true,
    "node": true,
    "es6": true,
    "mocha": true,
    "jest": true,
  },
  "globals": {
    "__DEV__": true,
    "__WECHAT__": true,
    "__ALIPAY__": true,
    "App": true,
    "Page": true,
    "Component": true,
    "wx": true,
    "getApp": true,
  },
  "rules": {
    'no-console': 'off',
  },
  "settings": {
    "import/resolver": {
      "webpack": {
        "config": "webpack.config.babel.js"
      }
    }
  }
};
